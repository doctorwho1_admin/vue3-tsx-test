import { defineComponent } from 'vue';
import { withInstall } from '../../utils/ComponentUtil';
import viteIcon from '../icon/vite.svg';

const Bar = defineComponent({
    name: 'Bar',
    setup() {
        return () => (
            <div>
                <img src={viteIcon} alt="测试图片"/>
                <div>周杰伦</div>
            </div>
        );
    }
});

export default withInstall(Bar);
