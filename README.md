# Vue 3 + TypeScript + Vite + Rollup

当前项目是为了测试`rollup`编译`tsx`，进行模块化打包，本项目可以作为外部依赖添加到`package.json`中使用
，类似于第三方组件 `antd`或`element`，当然本项目是业余的，做个参考即可

***声明：项目代码是我多方借鉴总结出来的，并不是原创。***
## 关键文件
1. `.babelrc`
2. `rollup.config.js`
3. `tsconfig.json`

## 重点
1. `rollup`对图片文件的编译
2. `tsx`对图片文件的解析

## 项目文件
进行测试的文件都放在了`packages`目录下

![img.png](img.png)

`bar`目录下的内容

内容其实很简单，只是一个测试而已

![img_1.png](img_1.png)
## 最终效果

![img_2.png](img_2.png)

编译后的文件目录结构

![img_3.png](img_3.png)
